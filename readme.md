<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Requirement

- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Composer

## Cara Install
- dari cmd/command prompt : masuk ke folder aplikasi
- ketik pada cmd : composer install 
- renamefile (.env.example)  menjadi (.env)
- sesuaikan koneksi database pada file .env
- ketik pada cmd : php artisan key:generate
- ketik pada cmd : php artisan migrate

## Penggunaan
- ketik pada cmd : php artisan serve
- ketik url yang keluar pada cmd ke browser : contoh 127.0.0.1:8000
- tambahkan user terlebih dahulu melalui menu register
- setelah berhasil register, and bisa mencoba login

